# 3.8_Coursera_IBM-Data-Analyst-Professional-Certificate
Learn the foundational skills required for an entry-level data analyst role through this nine course Professional Certificate from IBM and position yourself competitively in the thriving job market for data analysts.

Learn the core principles of data analysis and gain hands-on skills practice. You’ll work with a variety of data sources, project scenarios, and data analysis tools, including Excel, SQL, Python, Jupyter Notebooks, and Cognos Analytics, gaining practical experience with data manipulation and applying analytical techniques.

1. C1-Introduction to Data Analytics
2. C2-Excel Basics for Data Analysis
3. C3-Data Visualization and Dashboard with Excel and Cognos
4. C4-Python for Data Science, AI & Development
5. C5-Python Project for Data Science
6. C6-Databases and SQL for Data Science with Python
7. C7-Data Analysis with Python
8. C8-Data Visualization with Python
9. C9-IBM Data Analyst Capstone Project
